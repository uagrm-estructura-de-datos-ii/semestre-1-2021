from ADT_BST.Arbol import Arbol
from tkinter import *

raiz = Tk()
raiz.mainloop()

arbol = Arbol()
arbol.insertar(50)
arbol.insertar(25)
arbol.insertar(75)
arbol.insertar(15)
arbol.insertar(35)
arbol.insertar(30)
arbol.insertar(40)
arbol.inOrden()
#print "Esta equilibrado ? " + str(arbol.estaEquilibrado())
#print "Suma de todos los Nodos = " + str(arbol.sumaNodo())
arbol.podar(35)
arbol.inOrden()
