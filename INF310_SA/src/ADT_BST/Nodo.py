class Nodo:
    __dato = 0
    __hijoIzquierdo = None
    __hijoDerecho = None
    
    
    def __init__(self, dato):
        self.__dato = dato
        self.__hijoIzquierdo = None
        self.__hijoDerecho = None
    
    def setDato(self, dato):
        if dato <= 0:
            print "Error no se puede aceptar una llave negativo"
            return;
        self.__dato = dato;

    def setHijoIzquierdo(self, hijoIzquierdo):
        self.__hijoIzquierdo = hijoIzquierdo

    def setHijoDerecho(self, hijoDerecho):
        self.__hijoDerecho = hijoDerecho

    def getDato(self):
        return self.__dato

    def getHijoIzquierdo(self):
        return self.__hijoIzquierdo

    def getHijoDerecho(self):
        return self.__hijoDerecho

    def cantidadHijos(self):
        a = 0
        if self.__hijoIzquierdo is not None:
            a = a + 1
        if self.__hijoDerecho is not None:
            a = a + 1
        return a

    def esHoja(self):
        if self.cantidadHijos() == 0:
            return True
        return False