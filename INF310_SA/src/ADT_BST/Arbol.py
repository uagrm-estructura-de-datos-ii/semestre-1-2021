from ADT_BST.Nodo import Nodo

class Arbol:
    __raiz = None
    __n = 0

    def __init__(self):
        self.__raiz = None
        self.__n = 0
    
    def insertar(self, x):
        #El arbol esta vacio?
        if self.__raiz is None:
            #El arbol esta vacio, por lo tanto lo construimo
            self.__raiz = Nodo(x)
        else:
            #Bucle que hace que el rojo vaya al abismo, el azul lo sigue por detras
            azul = None
            rojo = self.__raiz
            while rojo is not None:
                azul = rojo
                if x < rojo.getDato():
                    rojo = rojo.getHijoIzquierdo()
                else:
                    if x > rojo.getDato():
                        rojo = rojo.getHijoDerecho()
                    else:
                        print("El elemento a insertar ya existe en el arbol")
                        return

            if x < azul.getDato():
                azul.setHijoIzquierdo(Nodo(x))
            else:
                azul.setHijoDerecho(Nodo(x))
        self.__n = self.__n + 1
        #Estas equilibrado? NO: equilibrate
        #if not self.estaEquilibrado():
            #self.__equilibrar()
            


    def longitud(self):
        return self.__n
    
    def inOrden(self):
        print("INORDEN")
        if self.__raiz is None:
            print("Arbol vacio")
        else:
            self.__inOrden1(self.__raiz)

    
    
    
    def __inOrden1(self, nodo):
        if nodo is not None:
            self.__inOrden1(nodo.getHijoIzquierdo())
            print("[ " + str(nodo.getDato()) + " ]")
            self.__inOrden1(nodo.getHijoDerecho())
    
    def estaEquilibrado(self):
        return self.__estaEquilibrado1(self.__raiz)
    
    def __estaEquilibrado1(self, nodo):
        if(nodo is None):
            return True
        
        if(nodo.esHoja()):
            return True
        
        factor = self.__factorEquilibrio(nodo)
        if not(factor == -1 or factor == 0 or factor == 1):
            return False
        
        estaBalanceadoHI = self.__estaEquilibrado1(nodo.getHijoIzquierdo())
        estaBalanceadoHD = self.__estaEquilibrado1(nodo.getHijoDerecho())
        return estaBalanceadoHI and estaBalanceadoHD
        
    def __altura1(self, nodo):
        if(nodo is None):
            return -1
        
        if(nodo.esHoja()):
            return 0
        alturaHI = self.__altura1(nodo.getHijoIzquierdo())
        alturaHD = self.__altura1(nodo.getHijoDerecho())
        if(alturaHI > alturaHD):
            return alturaHI+1
        return alturaHD+1
        
    def __factorEquilibrio(self, nodo):
        if nodo is None:
            return 0
        alturaHI = self.__altura1(nodo.getHijoIzquierdo())
        alturaHD = self.__altura1(nodo.getHijoDerecho())        
        return alturaHD - alturaHI;
    
    def __rotacionSimpleIzquierdo(self, nodo):
        nodoHD = nodo.getHijoDerecho()
        nodoHDHI = nodoHD.getHijoIzquierdo();
        nodoHD.setHijoIzquierdo(nodo)
        nodo.setHijoDerecho(nodoHDHI)
        return nodoHD

    def __rotacionSimpleDerecho(self, nodo):
        nodoHI = nodo.getHijoIzquierdo()
        nodoHIHD = nodoHI.getHijoDerecho()
        nodoHI.setHijoDerecho(nodo)
        nodo.setHijoIzquierdo(nodoHIHD)
        return nodoHI
    
    def __rotacionDobleIzquierdo(self, nodo):
        nodo.setHijoDerecho( self.__rotacionSimpleDerecho(nodo.getHijoDerecho()))
        return self.__rotacionSimpleIzquierdo(nodo)
        
    def __rotacionDobleDerecho(self, nodo):
        nodo.setHijoIzquierdo( self.__rotacionSimpleIzquierdo(nodo.getHijoIzquierdo()))
        return self.__rotacionSimpleDerecho(nodo)
    
    def __equilibrar(self):
        self.__raiz = self.__equilibrar1(self.__raiz)
    
    def __equilibrar1(self, nodo):
        if(nodo is None):
            return None
        if(nodo.esHoja()):
            return nodo
        nodo.setHijoIzquierdo(self.__equilibrar1(nodo.getHijoIzquierdo()))
        nodo.setHijoDerecho(self.__equilibrar1(nodo.getHijoDerecho()))
        
        feq = self.__factorEquilibrio(nodo)
        feqHI = self.__factorEquilibrio(nodo.getHijoIzquierdo())
        feqHD = self.__factorEquilibrio(nodo.getHijoDerecho())
        if(feq == -2 and feqHI != 1):
            nodo = self.__rotacionSimpleDerecho(nodo)
        if(feq == 2 and feqHD != -1):
            nodo = self.__rotacionSimpleIzquierdo(nodo)
        if(feq == 2 and feqHD == -1):
            nodo = self.__rotacionDobleIzquierdo(nodo)
        if(feq == -2 and feqHI == 1):
            nodo = self.__rotacionDobleDerecho(nodo)
        return nodo


    def sumaNodo(self):
        return self.__sumaNodo1(self.__raiz)
    
    def __sumaNodo1(self, p):
        if p is None:
            return 0
        if p.esHoja():
            return p.getDato()
        cantHI = self.__sumaNodo1(p.getHijoIzquierdo())
        cantHD = self.__sumaNodo1(p.getHijoDerecho())
        return cantHI + cantHD + p.getDato()
    
    def podar(self, x):
        self.__raiz = self.__podar1(self.__raiz, x)
    
    def __podar1(self, p, x):
        if p is None:
            return None
        if p.esHoja():
            if p.getDato() == x:
                return None
        if p.getDato() == x:
            return None
        p.setHijoIzquierdo( self.__podar1(p.getHijoIzquierdo(), x) )
        p.setHijoDerecho( self.__podar1(p.getHijoDerecho(), x) )
        return p
    
    def eliminar(self, x):
        self.__raiz = self.__eliminar1(self.__raiz, x)
    
    def __eliminar1(self, p, x):
        if p is None:
            return None
        
        if p.esHoja():
            if p.getDato() == x:
                return None
        
        if p.cantHijos() == 1:
            if p.getDato() == x:
                if p.getHijoIzquierdo() is not None:
                    return p.getHijoIzquierdo()
                else:
                    return p.getHijoDerecho()
        
        if p.cantHijos() == 2:
            if p.getDato() == x:
                nuevoX = self.__prefijo()
                p.setDato( nuevoX )
                p.setHijoIzquierdo( self.__eliminar1( p.getHijoIzquierdo(), nuevoX) )
        
        p.setHijoIzquierdo( self.__eliminar1(p.getHijoIzquierdo, x) )
        p.setHijoDerecho( self.__eliminar1(p.getHijoDerecho, x) )
        return p









