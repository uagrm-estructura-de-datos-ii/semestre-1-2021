from Primer_Parcial.Arbol import Arbol

arbol = Arbol("Animal")
arbol.insertar("Animal", "Ave")
arbol.insertar("Animal", "Terrestre")
arbol.insertar("Terrestre", "Mamifero")
arbol.insertar("Ave", "Oviparo")

animal = raw_input("Que especie de animal es: ")

x = raw_input("Es Ave o Terrestre: ")
if x == "Ave":
    y = raw_input("Es Oviparo, si o no: ")
    if y == "si":
        arbol.insertar2(animal, x, "Oviparo")
    else:
        print "No se puede insertar"
else:
    if x == "Terrestre":
        y = raw_input("Es Mamifero, si o no: ")
        if y == "si":
            arbol.insertar2(animal, x, "Mamifero")
        else:
            print "No se puede insertar"

b = raw_input("Que animal buscas: ")
ruta = arbol.ruta(b)
print ruta
arbol.inOrden()
