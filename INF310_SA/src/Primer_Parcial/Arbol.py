from Primer_Parcial.Nodo import Nodo

class Arbol:
    __raiz = None
    __n = 0

    def __init__(self, dato):
        self.__raiz = Nodo(dato)
        self.__n = 1
    
    def __fetch(self, nodo, padre):
        if nodo == None:
            return None
        
        if nodo.esHoja():
            if nodo.getDato() == padre:
                return nodo
            
        if nodo.getDato() == padre:
            return nodo
        
        nodoBuscado = self.__fetch(nodo.getHijoIzquierdo(), padre)
        if nodoBuscado is not None:
            return nodoBuscado
        return self.__fetch(nodo.getHijoDerecho(), padre)
        
    
    def insertar(self, padre, x):
        nodoPadre = self.__fetch(self.__raiz, padre)
        if nodoPadre is None:
            print "No se puede insertar a X porque padre no existe en el arbol"
            return
        
        if nodoPadre.getHijoIzquierdo() is None:
            nodoPadre.setHijoIzquierdo( Nodo(x) )
            return
        
        if nodoPadre.getHijoDerecho() is None:
            nodoPadre.setHijoDerecho( Nodo(x) )
            return
        
        print "No se puede insertar X debido a que el padre esta lleno"

    def insertar2(self, x, padre1, padre2):
        nodo = self.__raiz
        
        ''' Ave'''
        if nodo.getHijoIzquierdo().getDato() == padre1:
            nodo = nodo.getHijoIzquierdo()
            ''' Oviparo'''
            if nodo.getHijoIzquierdo().getDato() == padre2:
                self.__insertarHijo(nodo.getHijoIzquierdo(), x)
            else:
                ''' Viviparo '''
                if nodo.getHijoDerecho().getDato() == padre2:
                    self.__insertarHijo(nodo.getHijoDerecho(), x)
        else: 
            if nodo.getHijoDerecho().getDato() == padre1:
                nodo = nodo.getHijoDerecho()
                if nodo.getHijoIzquierdo().getDato() == padre2:
                    self.__insertarHijo(nodo.getHijoIzquierdo(), x)
                else:
                    if nodo.getHijoDerecho().getDato() == padre2:
                        self.__insertarHijo(nodo.getHijoDerecho(), x)
                
    def ruta(self, dato):
        return self.__ruta1(self.__raiz, dato)
    
    def __ruta1(self, nodo, dato):
        if nodo is None:
            return None
        
        if nodo.getDato() == dato:
            return dato
        
        rutaHI = self.__ruta1(nodo.getHijoIzquierdo(), dato)
        if rutaHI is not None:
            return nodo.getDato() + "/" + rutaHI
        
        rutaHD = self.__ruta1(nodo.getHijoDerecho(), dato)
        if rutaHD is not None:
            return nodo.getDato() + "/" + rutaHD
        
        return None
                
    def __insertarHijo(self, nodoPadre, x):
        if nodoPadre.getHijoIzquierdo() is None:
            nodoPadre.setHijoIzquierdo( Nodo(x) )
            return
        
        if nodoPadre.getHijoDerecho() is None:
            nodoPadre.setHijoDerecho( Nodo(x) )
            return
        
        print "No se puede insertar X debido a que el padre esta lleno"
        
        
    def longitud(self):
        return self.__n
    
    def inOrden(self):
        print("INORDEN")
        if self.__raiz is None:
            print("Arbol vacio")
        else:
            self.__inOrden1(self.__raiz)

    def __inOrden1(self, nodo):
        if nodo is not None:
            self.__inOrden1(nodo.getHijoIzquierdo())
            print("[ " + str(nodo.getDato()) + " ]")
            self.__inOrden1(nodo.getHijoDerecho())
    








