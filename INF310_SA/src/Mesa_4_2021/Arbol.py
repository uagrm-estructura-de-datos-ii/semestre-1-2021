from Nodo import Nodo

class Arbol:    
    def __init__(self, M):
        self.__M = M
        self.__raiz = None
        
    def __existe(self, dato, p):
        if p == None:
            return False
        else:
            if p.existe(dato):
                return True
         
        ph = p.getPosHijo(dato)
        return  self.__existe( dato, p.getHijo( ph ) )
    
    def insertar(self, dato, padre):
        if self.__raiz == None:
            self.__raiz = Nodo(self.__M, dato)
        else:    
            '''if not self.__existe(dato, self.__raiz): '''
            self.__raiz = self.__insertar1(self.__raiz, dato, padre)
            '''else:
                print('El dato ya existe')'''

    def __insertar1(self, p, dato, padre):
        if p == None:
            return None
        else:
            if p.getData() == padre:
                print 'se agrego ', dato, ' al padre ', padre
                p.agregarHijo(Nodo(self.__M, dato))
                return p
            else:
                for ph in range(p.cantHijos()):                    
                    p.setHijo( self.__insertar1(p.getHijo(ph), dato, padre), ph )
                return p
    
    def preOrden(self):
        self.__preOrden1(self.__raiz)

    def __preOrden1(self, p):
        if p != None:
            print p.getData()
            for i in range(p.cantHijos()):
                self.__preOrden1(p.getHijo( i ))            