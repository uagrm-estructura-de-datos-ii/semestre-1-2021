class Nodo:
    
    def __init__(self, M, D):
        self.M = M
        self.D = D
        self.P = 0
        self.H = []
        
        #M = 3
        for x in range(self.M):            
            self.H.append(None)              
    
    def getData(self):
        return self.D

    def setData(self,d ):
        self.D = d

    def getHijo(self,i):
        return self.H[i]

    def setHijo(self, p , i):
        self.H[i] = p        
    
    def agregarHijo(self, p):
        self.H[self.P] = p
        self.P = self.P + 1
    
    def esHoja(self):
        for i in range(self.M):
            if self.H[i] is not None:
                return False
        return True
        
    def cantHijos(self):
        c = 0
        for i in range(self.M):
            if self.H[i] is not None:
                c = c + 1
        return c