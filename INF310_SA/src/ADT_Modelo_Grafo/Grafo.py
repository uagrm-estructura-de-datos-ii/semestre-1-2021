from ADT_Modelo_Grafo.Ciudad import Ciudad
from ADT_Modelo_Grafo.Nodo import Nodo

class Grafo:
    MAX_VERTICE = 100
    ciudades = []
    n = 0
    __marca = []
    
    def __init__(self):
        self.ciudades = []
        self.n = 0
        self.__construirMarca()
        self.__desmarcarTodo()
    
    def agregarVertice(self, pais, ciudad):
        c = Ciudad(pais, ciudad, self.n)
        self.ciudades.append(c)
        self.n = self.n + 1
    
    def agregarArista(self, indiceOrigen, indiceDestino, peso):
        c = self.__buscarCiudad(indiceOrigen)
        if c is not None:
            c.agregarArista(indiceDestino, peso)
    
    
    def __buscarCiudad(self, indice):
        for c in self.ciudades:
            if c.indice == indice:
                return c
        return None
    
    def imprimir(self):
        for c in self.ciudades:
            s = str(c.indice) + "-" + c.pais + "/" + c.ciudad + " = "
            
            aristas = c.getAristas()
            for a in aristas:
                s = s + str(a.indice) + "/" + str(a.peso)+ ", "
            
            print(s)
    
    
    def dfs(self, indiceOrigen):
        self.__desmarcarTodo()
        print("DFS:")
        self.__dfs1(indiceOrigen)
        print("FIN::DFS")
        return
    
    def __dfs1(self, u):
        print(str(u))
        self.__marcar(u)
        
        c = self.__buscarCiudad(u)
        for a in c.getAristas():
            v = a.indice
            if(self.__estaMarcado(v) == False):
                self.__dfs1(v)
                
    def alcanzable(self, indiceOrigen, indiceDestino):
        self.__desmarcarTodo()
        print("Alcanzable:")
        loEncontro = self.__alcanzable1(indiceOrigen, indiceDestino)
        if loEncontro == True:
            print("Si es alcanzable")
        else:
            print("No es alcanzable")
        print("FIN::Alcanzable")
        
    
    def __alcanzable1(self, u, w):
        c = self.__buscarCiudad(u)        
        self.__marcar(u)
              
        for a in c.getAristas():
            v = a.indice
            if v == w:
                return True
                
            if(self.__estaMarcado(v) == False):
                loEncontro = self.__alcanzable1(v, w)
                if loEncontro == True:
                    cv = self.__buscarCiudad(v)
                    print( cv.ciudad )
                    return True
        return False
    
    def __esValidoVertice(self, v):
        if v >= 0 and v <= self.n:
            return True
        print("Error, vertice invalido")
        return False
    
    
    def __construirMarca(self):
        for x in range(self.MAX_VERTICE):
            self.__marca.append(False)
    
    def __desmarcarTodo(self):
        for x in range(self.MAX_VERTICE):
            self.__marca[x] = False
    
    def __marcarTodo(self):
        for x in range(self.MAX_VERTICE):
            self.__marca[x] = True
    
    def __marcar(self, x):
        if self.__esValidoVertice(x):
            self.__marca[x] = True
    
    def __desmarcar(self, x):
        if self.__esValidoVertice(x):
            self.__marca[x] = False      
       
    def __estaMarcado(self, x):
        return self.__marca[x]
