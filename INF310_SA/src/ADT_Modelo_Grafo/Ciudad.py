from ADT_Modelo_Grafo.Nodo import Nodo

class Ciudad:
    indice = 0
    pais = ""
    ciudad = ""
    aristas = []
    
    def __init__(self, pais, ciudad, indice):
        self.indice = indice
        self.pais = pais
        self.ciudad = ciudad
        self.aristas = []
    
    def agregarArista(self, indiceDestino, peso):
        self.aristas.append(Nodo(indiceDestino, peso))
    
    def getAristas(self):
        return self.aristas
    
    
    
        
    
    