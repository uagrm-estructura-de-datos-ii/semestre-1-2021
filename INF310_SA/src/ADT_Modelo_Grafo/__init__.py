from ADT_Modelo_Grafo.Grafo import Grafo

grafo = Grafo()
grafo.agregarVertice("Bolivia", "Santa Cruz")
grafo.agregarVertice("Bolivia", "Cochabamba")
grafo.agregarVertice("Bolivia", "La Paz")
grafo.agregarVertice("Brasil", "Sao Paulo")
grafo.agregarVertice("Peru", "Cusco")
grafo.agregarVertice("Argentina", "Plata")

grafo.agregarArista(0, 1, 50)
grafo.agregarArista(0, 3, 350)
grafo.agregarArista(0, 5, 250)
grafo.agregarArista(1, 2, 30)
grafo.agregarArista(2, 4, 150)

grafo.imprimir()

grafo.dfs(2)
grafo.alcanzable(0, 4)