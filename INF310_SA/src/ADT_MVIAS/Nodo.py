# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
class Nodo:
    
    def __init__(self, M, D):
        self.M = M
        self.D = []
        self.H = []
        #M = 3
        for x in range(self.M-1): #for x in range(2)
            self.D.append(-1)       #x=0,1 entra 2 veces
            self.H.append(None)
        self.H.append(None)#M=3 M[3] = None
        self.D[0] = D
    
    def getData(self,i):
        return self.D[i]

    def setData(self,d , i):
        self.D[i] = d

    def getHijo(self,i):
        return self.H[i]

    def setHijo(self,p , i):
        self.H[i] = p
    
    def existe(self, dato):
        for i in range(self.M-1):
            if self.D[i] == dato:
                return True
        return False
    
    def esHoja(self):
        for i in range(self.M):
            if self.H[i] is not None:
                return False
        return True
    
    def cantDatos(self):
        c = 0
        for i in range(self.M -1 ):
            if self.D[i] != -1:
                c = c + 1
        return c

    def lleno(self):
        if self.cantDatos() == self.M-1:
            return True
        return False
    
    def getPosHijo(self, dato):
        for i in range(self.M -1 ):
            if self.D[ i ] > dato:
                return i
        return self.M-1

    def getPosLibre(self):
        if self.lleno():
            return -1
        for i in range(self.M -1 ):
            if self.D[i] == -1:
                return i
    
    def insertar(self, dato):
        pos = self.getPosLibre()
        while (pos > 0) and (self.getData(pos-1) > dato) :
            self.setData( self.getData( pos-1 ) , pos )
            pos = pos - 1
        self.setData(dato, pos)

