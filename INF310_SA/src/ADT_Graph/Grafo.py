# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from ADT_Graph.Lista import Lista

class Grafo:
    __MAXVERTICE = 100
    __n = 0    
    __V = []
    __marca = []
    
    def __init__(self):
        self.__n = -1   
        self.__V = []
        self.__construirMarca()
        self.__desmarcarTodo()
        
    def agregarVertice(self):
        if self.__n < self.__MAXVERTICE:
            self.__V.append(Lista())
            self.__n = self.__n + 1
        else:
            print("Error, no se puede agregar mas vertice")
    
    def agregarArista(self, u, peso, v):
        if self.__esValidoVertice(u) and self.__esValidoVertice(v):
            if peso < 0:
                print("Error, no se puede agregar un peso negativo")
                return
            self.__V[u].add(v, peso)
            
    
    def imprimir(self):        
        for x in range(self.__n+1):
            print(str(x) + " -> " + self.__V[x].toString())
    
    def dfs(self, u):
        self.__desmarcarTodo()
        print("DFS:")
        self.__dfs1(u)
        print("FIN::DFS")
        return
    
    def __dfs1(self, u):
        print(str(u))
        self.__marcar(u)
        for x in range(self.__V[u].length()):
            v = self.__V[u].get(x)
            if(self.__estaMarcado(v) == False):
                self.__dfs1(v)        
    
    def __esValidoVertice(self, v):
        if v >= 0 and v <= self.__n:
            return True
        print("Error, vertice invalido")
        return False
    
    def __construirMarca(self):
        for x in range(self.__MAXVERTICE):
            self.__marca.append(False)
    
    def __desmarcarTodo(self):
        for x in range(self.__MAXVERTICE):
            self.__marca[x] = False
    
    def __marcarTodo(self):
        for x in range(self.__MAXVERTICE):
            self.__marca[x] = True
    
    def __marcar(self, x):
        if self.__esValidoVertice(x):
            self.__marca[x] = True
    
    def __desmarcar(self, x):
        if self.__esValidoVertice(x):
            self.__marca[x] = False      
       
    def __estaMarcado(self, x):
        return self.__marca[x]