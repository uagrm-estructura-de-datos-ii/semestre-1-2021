package ADT_Lista.Examenes.PrimeroParcial.A_H_2_2021.Arbol;

import java.util.LinkedList;

public class Arbol {
    private Nodo Raiz;
    
    public Arbol(int padre){        //Crea el árbol con raíz=padre
       Raiz = new Nodo(padre);
    }
    
    public boolean estaXNivel(int x, int nivel) {
        return estaXNivel(Raiz, x, nivel, 1);
    }
    
    private boolean estaXNivel(Nodo p, int x, int nivel, int n) {
        if(p == null)
            return false;
        if(hoja(p)) {
            if(p.getData() == x) {
                if(nivel == 0){
                    return true;
                }
                return false;
            }
        }
        
        if(p.getData() == x) {
            if(nivel == 0) {
                return true;
            }
            return false;
        }
        
        if(estaXNivel(p.getHI(), x, nivel, n+1) == true)
            return true;
        if(estaXNivel(p.getHD(), x, nivel, n+1) == true)
            return true;
        return false;
    }
    
    private Nodo delIncompleto(Nodo p, int x) {
        if(p == null)
            return null;
        if(hoja(p))
            return p;
        
        if(p.getData() == x) {
            if(p.cantHijos() == 1) {
                if(p.getHI() != null)
                    return p.getHI();
                return p.getHD();
            } else {
                return p;
            }   
        }
        
        p.setHI( delIncompleto(p.getHI(), x) );
        p.setHD( delIncompleto(p.getHD(), x) );
        return p;
    }
    
    public boolean noExisteIncompleto() {
        if(Raiz == null)
            return false;
        return noExisteIncompleto(Raiz);
    }
    
    private boolean noExisteIncompleto(Nodo p) {
        if(p == null)
            return true;
        if(hoja(p))
            return true;
        
        if(p.cantHijos() == 1)
            return false;
        
        if(noExisteIncompleto(p.getHI())==false)
            return false;
        if(noExisteIncompleto(p.getHD())==false)
            return false;
        return true;
    }
    
    public int cantIncompleto() {
        return cantIncompleto(Raiz);
    }
    
    private int cantIncompleto(Nodo p) {
        if(p == null)
            return 0;
        if(hoja(p))
            return 0;
        
        int ci = cantIncompleto(p.getHI());
        int cd = cantIncompleto(p.getHD());
        if(p.cantHijos() == 1) {
            return ci + cd + 1;
        }
        return ci + cd;
    }
    
   
    private boolean hoja(Nodo T){
        return (T != null && T.cantHijos() == 0);
    }
    public void add(int padre, int x){ //Inserta x al arbol, como hijo de padre. 
        if(fetch(Raiz, x) == null) {
            Nodo p = fetch(Raiz, padre);
            if(p != null) {
                if(x % 2 == 0) {
                    if(p.getHD() == null) {
                        Nodo aux = new Nodo(x);
                        p.setHD(aux);
                    }else {
                        p.getHD().setData(x);
                    }
                } else {
                    if(p.getHI() == null) {
                        Nodo aux = new Nodo(x);
                        p.setHI(aux);
                    }else {
                        p.getHI().setData(x);
                    }
                }
            }
        }
    }
    private Nodo fetch(Nodo p, int x){  //Fetch al nodo cuyo Data=x.
        if(p == null)
            return null;
        if(p.cantHijos() == 0) {
            if(p.getData() == x) {
                return p;
            }
        }
        
        //caso cuando p tiene almenos 1 hijo.
        if(p.getData() == x) {
            return p;
        }
        Nodo nodoX = fetch(p.getHI(), x);
        if(nodoX == null) {
            return fetch(p.getHD(), x);
        }
        return nodoX;
    }
    
    public void Inorden(){
        if (Raiz == null)
            System.out.println("(Arbol vacío)");
        else{
            System.out.print("Inorden : ");
            Inorden(Raiz);
            System.out.println();
        }
    } 
     
    private void Inorden(Nodo T){
        if (T != null){
            Inorden(T.getHI());
            System.out.print(T.getData()+" ");
            Inorden(T.getHD());
        }
    }
    
    public void Preorden(){
        System.out.print("Preorden : ");
        
        if (Raiz == null)
            System.out.println("(Arbol vacío)");
        else{
            Preorden(Raiz);
            System.out.println();
        }
    }
    
    private void Preorden(Nodo T){
        if (T != null){
            System.out.print(T.getData() + " ");
            Preorden(T.getHI());
            Preorden(T.getHD());
        }
    }
    
    
    
    
    public void niveles(){ 
        System.out.print("Niveles: ");
        
        if (Raiz == null)
            System.out.println("(Arbol vacío)");
        else{
            niveles(Raiz);
        }    
    }
   
    
//---------- Métodos auxiliares para mostrar el árbol por niveles --------------
    private void niveles(Nodo T){   //Pre: T no es null.
        LinkedList <Nodo> colaNodos   = new LinkedList<>();
        LinkedList<Integer> colaNivel = new LinkedList<>();
        
        int nivelActual = 0;
        String coma="";
        
        colaNodos.addLast(T);
        colaNivel.addLast(1);
        
        do{
            Nodo p = colaNodos.pop();       //Sacar nodo de la cola
            int nivelP = colaNivel.pop();
            
            if (nivelP != nivelActual){ //Se está cambiando de nivel
                System.out.println();
                System.out.print("  Nivel "+nivelP+": ");
                nivelActual = nivelP;
                coma = "";
            }
            
            System.out.print(coma + p);
            coma = ", ";
            
            addHijos(colaNodos, colaNivel, p, nivelP);   
        }while (colaNodos.size() > 0);
        
        System.out.println();
    }
    
    private void addHijos(LinkedList <Nodo> colaNodos, LinkedList<Integer> colaNivel,  Nodo p, int nivelP){
        for (int i=1; i<=Nodo.M; i++){  //Insertar a la cola de nodos los hijos no-nulos de p
            Nodo hijo = p.getHijo(i);
            
            if (hijo != null){
                colaNodos.addLast(hijo);
                colaNivel.addLast(nivelP+1);
            }
        }
    }

}
