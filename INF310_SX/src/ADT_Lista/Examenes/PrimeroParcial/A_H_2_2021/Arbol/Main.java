package ADT_Lista.Examenes.PrimeroParcial.A_H_2_2021.Arbol;

public class Main {
    
    public static void main(String[] args) {  
        Arbol A = new Arbol(50);
        
        A.add(50,25);
        A.add(25,60);
        A.add(50,80);
        A.add(100,80);
        A.add(80,25);
        A.add(80,40);
        A.add(25,15);
        A.add(50,35);
        A.add(80,20);
        
        A.Inorden();
        A.niveles();
    }
    
}
