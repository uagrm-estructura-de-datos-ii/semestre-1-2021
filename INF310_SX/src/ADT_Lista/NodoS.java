package ADT_Lista;

public class NodoS {
    private int dato;
    private NodoS enlace;

    public NodoS() {
        dato = -1;
        enlace = null;
    }

    public NodoS(int dato) {
        this.dato = dato;
        enlace = null;
    }
    
    public NodoS(NodoS nodo) {
        dato = nodo.getDato();
        enlace = nodo.getEnlace();
    }

    public int getDato() {
        return dato;
    }

    public void setDato(int dato) {
        this.dato = dato;
    }

    public NodoS getEnlace() {
        return enlace;
    }

    public void setEnlace(NodoS enlace) {
        this.enlace = enlace;
    }
    
    
}
