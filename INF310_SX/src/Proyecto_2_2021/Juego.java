/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto_2_2021;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ronaldo Rivero
 */
public class Juego {
    
    int raton;
    int inicio, salida;
    List<Cueva> cuevas;

    public Juego() {
        raton = -1;
        cuevas = new ArrayList<Cueva>();
    }
    
    public void agregarCueva(int x, int y) {
        int indice = cuevas.size();
        Cueva c = new Cueva(indice, x, y);        
        cuevas.add(c);
    }
    
    public void agregarTunel(int u, int v, boolean tieneTrampa) {
        for (Cueva cueva : cuevas) {
            if(cueva.indice == u) {
                cueva.agregarTunel(new Tunel(v, tieneTrampa));
            }
        }
    }
    
    public void cuevaInicio(int indice) {
        Cueva cueva = buscarCueva(indice);
        if(cueva != null) {
            cueva.marcarInicio();
            inicio = indice;
            raton = indice;
        }
    }
    
    public void cuevaSalida(int indice) {
        for (Cueva cueva : cuevas) {
            if(cueva.indice == indice) {
                cueva.marcarSalida();
            }
        }
        salida = indice;
    }
    
    public void moverRaton(int indice) {
        Cueva cueva = buscarCueva(raton);
        if(cueva != null) {
            
            for(Tunel t : cueva.tuneles) {
                if(t.destino == indice) {
                    if(t.tieneTrampa() == true) {
                        System.err.println("Game Over, caiste en una trampa");
                        return;
                    } else {
                        raton = indice;
                        System.out.println("Movimiento con exito");
                        return;
                    }
                }
            }
            System.err.println("el indice enviado es incorrecto, no existe un tunel hacia esa cueva");
        } 
    }
    
    public boolean ganoRaton() {
        return raton == salida;
    }
    
    //CASO N3
    public void alcanzable() {
        alcazable(inicio, salida);
    }
    
    private void alcazable(int v, int u) {
        
    }
    //FIN::CASO N3
    
    private Cueva buscarCueva(int indice) {
        if(indice < 0 || indice >= cuevas.size()) {
            System.err.println("Indice invalido, no exista una cueva con ese indice");
            return null; 
        }
        
        for (Cueva cueva : cuevas) {
            if(cueva.indice == indice) {
                return cueva;
            }
        }
        return null;
    }
}
