/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto_2_2021;

/**
 *
 * @author Ronaldo Rivero
 */
public class Tunel {
    int destino;
    boolean tieneTrampa;

    public Tunel(int destino) {
        this.destino = destino;
        tieneTrampa = false;
    }
    
    public Tunel(int destino, boolean tieneTrampa) {
        this.destino = destino;
        this.tieneTrampa = tieneTrampa;
    }
    
    public boolean tieneTrampa() {
        return tieneTrampa;
    }
}
