/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto_2_2021;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ronaldo Rivero
 */
public class Cueva {
    
    int indice;
    int tipo;//0 inicio, 1 nada, 2 salida
    float x, y;
    List<Tunel> tuneles;

    public Cueva() {
        tipo = 1;
        x = y = 0;
        tuneles = new ArrayList<Tunel>();
    }
    
    public Cueva(int indice) {
        tipo = 1;
        this.indice = indice;
        x = y = 0;
        tuneles = new ArrayList<Tunel>();
    }
    
    public Cueva(int indice, int x, int y) {
        tipo = 1;
        this.indice = indice;
        this.x = x;
        this.y = y;
        tuneles = new ArrayList<Tunel>();
    }
    
    public void marcarInicio() {
        tipo = 0;
    }
    
    public void marcarSalida() {
        tipo = 2;
    }
    
    public void marcarVacio() {
        tipo = 1;
    }
    
    public boolean esSalida() {
        return tipo == 2;
    }

    public void agregarTunel(Tunel tunel) {
        tuneles.add(tunel);
    }
}
