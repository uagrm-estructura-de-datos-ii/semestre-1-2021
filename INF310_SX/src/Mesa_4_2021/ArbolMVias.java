/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mesa_4_2021;
public class ArbolMVias {
    private int M;
    private Nodo raiz;
    
    public ArbolMVias(int M) {
        this.M = M;
        this.raiz = null;
    }
    
    public void insertar(String padre, String hijo) {
        if(this.raiz == null)
            this.raiz = new Nodo(M, hijo);
        else
            this.raiz = insertar(raiz, padre, hijo);
    }

    private Nodo insertar(Nodo p, String padre, String hijo) {
        if(p == null)
            return null;
        if (p.getDato().equals(padre)) {
            p.setHijo(new Nodo(M, hijo));
        } else {
            for(int i = 0; i < p.cantHijos(); i++) {
                p.setHijo(insertar(p.getHijo(i), padre, hijo), i);
            }
            return p;
        }
        return p;
    }
    
    public void preOrden() {
        preOrden(raiz);
    }
        

    private void preOrden(Nodo p) {
        if(p == null)
            return;
        System.out.println(p.getDato());
        for(int i = 0; i < p.cantHijos(); i++) {
            preOrden(p.getHijo(i));
        }
    }
}
