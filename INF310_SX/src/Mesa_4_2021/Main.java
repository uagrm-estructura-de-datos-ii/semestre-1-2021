/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mesa_4_2021;


public class Main {

    public static void main(String[] args) {
        ArbolMVias arbol = new ArbolMVias(4);
        arbol.insertar(null, "root");
        arbol.insertar("root", "c:/");
        arbol.insertar("c:/", "Programas");
        arbol.insertar("c:/", "MisDoc");
        arbol.insertar("c:/", "Win32");
        arbol.insertar("Programas", "Python");
        arbol.insertar("Programas", "Java");
        arbol.insertar("Programas", "C++");
        arbol.insertar("MisDoc", "Imagenes");
        arbol.insertar("Imagenes", "img.bmp");
        arbol.preOrden();
    }
    
}
