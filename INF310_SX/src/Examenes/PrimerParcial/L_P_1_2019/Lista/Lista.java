package Examenes.PrimerParcial.L_P_1_2019.Lista;

import Examenes.PrimerParcial.*;
import ADT_Lista.*;

/**
 *
 * @author Ronaldo Rivero
 */
public class Lista {
    private Nodo L;
    private int n;
    
    /**
     * Constructor de una Lista vacía
     */
    public Lista() {
        L = null;
        n = 0;
    }
    
    /**
     * Inserta el elemento X en la lista de manera ordenada con
     * el criterio ascendente
     * @param x int
     */
    public void insertar(int id, int valor) {
        if(L == null) {            
            L = new Nodo(id);
            Nodo v = new Nodo(valor);
            L.setEnlace(v);           
            n = 1;
        } else {
            Nodo azul = L;
            Nodo rojo = null;                    
            while(azul != null && azul.getDato() <= id) {                                                       
                if(azul.getDato() == id) {
                    Nodo s = azul.getEnlace();
                    s.setDato(valor);
                    return;
                }         
                rojo = azul;
                azul = azul.getEnlace().getEnlace();
            }
            
            if(rojo == null) {
                Nodo aux = new Nodo(id);
                Nodo aux2 = new Nodo(valor);
                aux.setEnlace(aux2);
                aux2.setEnlace(L);                                
                L = aux;                
            } else if(rojo != null && azul != null) {
                Nodo aux = new Nodo(id);
                Nodo aux2 = new Nodo(valor);
                aux.setEnlace(aux2);
                aux2.setEnlace(azul);  
                
                rojo = rojo.getEnlace();
                rojo.setEnlace(aux);                
            } else if(azul == null) {
                Nodo aux = new Nodo(id);
                Nodo aux2 = new Nodo(valor);
                aux.setEnlace(aux2);
                
                rojo = rojo.getEnlace();                
                rojo.setEnlace(aux);
            }
            n = n + 1;
        }
    }    
    
    public int longitud() {
        return n;
    }
    
 
    /**
     * Imprimir la Lista
     * @return 
     */
    @Override
    public String toString() {
        String s = "[";
        Nodo aux = L;
        while(aux != null) {
            s = s + aux.getDato() + " | ";
            aux = aux.getEnlace();
        }
        return s + "]";
    }
}
