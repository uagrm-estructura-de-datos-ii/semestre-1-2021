package Examenes.PrimerParcial.G_O_1_2018.Lista;


public class Main {
    private static final int[] datos = {11, 9, 5, 3, 12, 8, 10};
    
    private static void cargarDatos(Lista L){
        for (int i = 0; i < datos.length; i++) {
            L.add(datos[i]);
        }
    }
    
    
    public static void main(String[] args) {
        Lista p = new Lista();
        cargarDatos(p);
        System.out.println( p );
        
        //[3, 5, 8, 9, 10, 11, 12]
        p.retroceder(8, 1);
        System.out.print("p.retroceder(8, 1) = ");
        System.out.println( p );
        //[3, 8, 5, 9, 10, 11, 12]
        p.retroceder(10, 3);
        System.out.print("p.retroceder(10, 3) = ");
        System.out.println( p );
        //[3, 10, 8, 5, 9, 11, 12]
        p.retroceder(50, 3);
        System.out.print("p.retroceder(50, 3) = ");
        System.out.println( p );
        //[3, 10, 8, 5, 9, 11, 12]
        
        p.retroceder(8, 5);
        System.out.print("p.retroceder(8, 5) = ");
        System.out.println( p );
        //[8, 3, 10, 5, 9, 11, 12]
    }  
}
