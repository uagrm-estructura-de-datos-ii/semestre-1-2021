package Examenes.PrimerParcial.L_Z_2_2021.Lista;

public class Lista {    //ADT Lista (Ordenada y sin duplicados).
    private Nodo L;     //Si L no es null, L apunta al ULTIMO nodo de la circular-list.  
    
    public Lista(){
        L = null;
    }
       
    public void add(int x){ //PREGUNTA 2. Inserta x a la Lista.
        if(L == null) { // no existe ningun elemento en la lista
            L = new Nodo(x);
            L.setLink(L);
            return;
        }
        if(L.getLink().equals(L)) { // solo existe un elemento en la lista
            Nodo nuevo = new Nodo(x);
            if(x < L.getData()) {
                nuevo.setLink(L);
                L.setLink(nuevo);
            } else if(x > L.getData()) {
                L.setLink(nuevo);
                nuevo.setLink(L);
                L = nuevo;
            }
            return;
        }
        // cuando existe mas de un elemento en la lista
        Nodo ant = null;
        Nodo p = L.getLink();
        while(p.equals(L) == false && x >= p.getData()) {
            if(p.getData() == x) // x existe en la lista
                return;
            ant = p;
            p = p.getLink();
        }
        Nodo nuevo = new Nodo(x);
        if(ant == null) {//x es el menor de toda la lista
            nuevo.setLink(p);
            L.setLink(nuevo);
            return;
        }
        if(p.equals(L) && x > L.getData()) { // x es el mayor de toda la lista
            nuevo.setLink(L.getLink());
            L.setLink(nuevo);
            L = nuevo;
            return;
        }
        //x esta entre nodo Ant y nodo P
        nuevo.setLink(p);
        ant.setLink(nuevo);
    }
    
    
    @Override
    public String toString(){ 
        if (L==null)
            return "[]";
                    
        String S = "[";
        String coma="";
        String u = ""+L.getData();
       
        Nodo p = L.getLink();
        while (p != L){
            S += coma + p.getData();
            coma=", ";
            p = p.getLink();
            if (p==null)
                return "[Error: La lista no es circular]";
        }
                   
       return S+coma+u+"]";
    }
    
}
