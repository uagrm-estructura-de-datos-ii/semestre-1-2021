package Examenes.PrimerParcial.L_Z_2_2021.Arbol;

import java.util.LinkedList;

public class Arbol {    //Binario y desordenado, pero sin duplicados.
    private Nodo Raiz;
    
    public Arbol(int padre){        //Crea el árbol con raíz=padre
       Raiz = new Nodo(padre);
    }
    
   public void addPadre(int papa, int x){  //PREGUNTA 1.
        if( fetch(Raiz, papa) == null && fetch(Raiz, x) != null) {
            Raiz = addPadre(Raiz, papa, x);
        }
   }
   
   private Nodo addPadre(Nodo p, int papa, int x) {
       if(p == null)
           return null;
       if(hoja(p)) {
           return p;
       }
       
       if(p.getHI() != null) {
           if(p.getHI().getData() == x) {
               Nodo nuevoPapa = new Nodo(papa);
               nuevoPapa.setHI(p.getHI());
               p.setHI(nuevoPapa);
               return p;
           }
       }
       if(p.getHD() != null) {
           if(p.getHD().getData() == x) {
               Nodo nuevoPapa = new Nodo(papa);
               nuevoPapa.setHD(p.getHD());
               p.setHD(nuevoPapa);
               return p;
           }
       }
       
       p.HI = addPadre(p.getHI(), papa, x);
       p.HD = addPadre(p.getHD(), papa, x);
       return p;
   }
    
   
    private boolean hoja(Nodo T){
        return (T != null && T.cantHijos() == 0);
    }
    
    
    public void add(int padre, int x, int indexHijo){ 
        Nodo p = fetch(Raiz, padre);
        if (p==null || fetch(Raiz, x) != null)
            return;     //El padre no existe o x ya está en el árbol.
              
        if (p.getHijo(indexHijo) == null)
            p.setHijo(indexHijo, new Nodo(x));
    } 
    
    private Nodo fetch(Nodo t, int x){  
        if (t==null || t.getData() == x)
            return t;       
              
        Nodo hi = fetch(t.getHI(), x);
        if (hi != null)
            return hi;
        
        return fetch(t.getHD(), x);
    }
    
    public void Inorden(){
        if (Raiz == null)
            System.out.println("(Arbol vacío)");
        else{
            System.out.print("Inorden : ");
            Inorden(Raiz);
            System.out.println();
        }
    } 
     
    private void Inorden(Nodo T){
        if (T != null){
            Inorden(T.getHI());
            System.out.print(T.getData()+" ");
            Inorden(T.getHD());
        }
    }
    
       
    public void niveles(){ 
        System.out.print("Niveles: ");
        
        if (Raiz == null)
            System.out.println("(Arbol vacío)");
        else{
            niveles(Raiz);
        }    
    }
   
    
//---------- Métodos auxiliares para mostrar el árbol por niveles --------------
    private void niveles(Nodo T){   //Pre: T no es null.
        LinkedList <Nodo> colaNodos   = new LinkedList<>();
        LinkedList<Integer> colaNivel = new LinkedList<>();
        
        int nivelActual = 0;
        String coma="";
        
        colaNodos.addLast(T);
        colaNivel.addLast(1);
        
        do{
            Nodo p = colaNodos.pop();       //Sacar nodo de la cola
            int nivelP = colaNivel.pop();
            
            if (nivelP != nivelActual){ //Se está cambiando de nivel
                System.out.println();
                System.out.print("  Nivel "+nivelP+": ");
                nivelActual = nivelP;
                coma = "";
            }
            
            System.out.print(coma + p);
            coma = ", ";
            
            addHijos(colaNodos, colaNivel, p, nivelP);   
        }while (colaNodos.size() > 0);
        
        System.out.println();
    }
    
    private void addHijos(LinkedList <Nodo> colaNodos, LinkedList<Integer> colaNivel,  Nodo p, int nivelP){
        for (int i=1; i<=Nodo.M; i++){  //Insertar a la cola de nodos los hijos no-nulos de p
            Nodo hijo = p.getHijo(i);
            
            if (hijo != null){
                colaNodos.addLast(hijo);
                colaNivel.addLast(nivelP+1);
            }
        }
    }

}
