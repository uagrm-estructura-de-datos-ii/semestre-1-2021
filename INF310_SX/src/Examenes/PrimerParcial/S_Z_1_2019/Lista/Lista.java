package Examenes.PrimerParcial.S_Z_1_2019.Lista;


/**
 *
 * @author Ronaldo Rivero
 */
public class Lista {
    private Nodo L;
    private int n;
    
    /**
     * Constructor de una Lista vacía
     */
    public Lista() {
        L = null;
        n = 0;
    }
    
    /**
     * Inserta el elemento X en la lista de manera ordenada con
     * el criterio ascendente
     * @param x int
     */
    public void insertar(int data, int peso) {
        if(L == null) {
            L = new Nodo(data, peso);
            n = 1;
        } else {
            Nodo p = L;
            Nodo ant = null;                    
            while(p != null && p.getDato() <= data) {                                                       
                if(p.getDato() == data && p.getPeso() == peso) {
                    System.err.println("No se pudo introducir "+data+" y peso "+peso+", porque ya existen");
                    return;
                }
                
                if(p.getDato() == data) {
                    if(peso < p.getPeso())
                        break;
                }           
                ant = p;
                p = p.getEnlace();
            }
            
            if(ant == null) {
                Nodo aux = new Nodo(data, peso);
                aux.setEnlace(L);
                L = aux;
            } else if(ant != null && p != null) {
                Nodo aux = new Nodo(data, peso);
                ant.setEnlace(aux);
                aux.setEnlace(p);
            } else if(p == null) {
                Nodo aux = new Nodo(data, peso);
                ant.setEnlace(aux);
            }
            n = n + 1;
        }
    }    
    
    public int longitud() {
        return n;
    }
    
 
    /**
     * Imprimir la Lista
     * @return 
     */
    @Override
    public String toString() {
        String s = "[";
        Nodo aux = L;
        while(aux != null) {
            s = s + aux.getDato() + ", " + aux.getPeso() + " | ";
            aux = aux.getEnlace();
        }
        return s + "]";
    }
}
