/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examenes.PrimerParcial.T2_2020.Arbol;

/**
 *
 * @author ronal
 */
public class Main {

    private static int data[] = {30, 20, 15, 10, 25, 22, 28, 60, 45, 40, 50, 70};
    private static int data2[] = {30, 20, 15, 10, 25, 22, 28, 40, 50};
    
    public static void main(String[] args) {
        
        Arbol bst = new Arbol();
        /*
        load(bst, data);
        bst.inOrden();
        bst.delHoja(25, 29);
        bst.delHoja(20, 28);
        bst.delHoja(20, 15);
        System.out.println("\ndelHoja(25,29)");
        System.out.println("delHoja(20,28)");
        System.out.println("delHoja(20,15)");
        bst.inOrden();       
        bst.delHoja(45, 40);
        bst.delHoja(15, 10);
        System.out.println("\ndelHoja(45,40)");
        System.out.println("delHoja(15,10)");
        bst.inOrden();
        */
        //Pregunta 3
        load(bst, data2);
        System.out.println("bst.dist(30, 5) = "+bst.dist(30, 5));
        System.out.println("bst.dist(32, 40) = "+bst.dist(32, 40));        
        System.out.println("bst.dist(20, 50) = "+bst.dist(20, 50));
        System.out.println("bst.dist(10, 20) = "+bst.dist(10, 20));
        System.out.println("bst.dist(30, 50) = "+bst.dist(30, 50));
        System.out.println("bst.dist(30, 10) = "+bst.dist(30, 10));
        System.out.println("bst.dist(25, 28) = "+bst.dist(25, 28));
    }
    
    private static void load(Arbol bst, int data[]) {
        for (int x : data) {
            bst.insertar(x);
        }
    }
    
    private static void imprimir(Arbol bst, String titulo) {
        System.out.println(titulo);
        bst.inOrden();
        System.out.println("\n");
    }
    
}
