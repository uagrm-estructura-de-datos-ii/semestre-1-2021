package Examenes.PrimerParcial.T2_2020.Lista;

/**
 *
 * @author Ronaldo Rivero
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Lista P = new Lista(); // P = [ ] (Lista vacía)
        P.add(3,5); // P = [ (3, 5) ] (Primera inserción)
        P.add(1,3); // P = [ (1,3), (3,5) ] porque (1, 3) < (3, 5)
        P.add(9,4); // P = [ (1,3), (3,5), (9, 4)] porque (9, 4) > (3, 5)
        P.add(9,2); // P = [ (1,3), (3,5), (9,2), (9, 4)] porque (9, 2) < (9, 4)
        P.add(3,5); // La Lista P queda igual, porque el Par (3, 5) ya está insertado.
        
        System.out.println(P);
    }
    
}
