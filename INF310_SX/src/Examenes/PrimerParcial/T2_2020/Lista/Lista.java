package Examenes.PrimerParcial.T2_2020.Lista;

/**
 *
 * @author Ronaldo Rivero
 */
public class Lista {
    private Nodo L;
 
    public Lista(){
        L = null;
    }
    
    public void add(int a, int b) {
        if(L == null){
            L = new Nodo(new Par(a,b));        
        } else {
            Nodo p = new Nodo(new Par(a,b));
            Nodo rojo = L;
            Nodo azul = null;
            //while(rojo != null && x >= rojo.getData())
            while(rojo != null && (p.mayor(rojo) || p.igual(rojo))) {
                azul = rojo;                
                if(p.igual(rojo))
                    return;                
                rojo = rojo.getLink();
            }
            
            if(azul == null && rojo != null) {
                p.setLink(rojo);
                L = p;
            } else {
                if(rojo == null && azul != null) {
                    azul.setLink(p);
                } else {
                    if(rojo != null && azul != null) {
                        p.setLink(rojo);
                        azul.setLink(p);
                    }
                }
            }
        }
    }
    
    @Override
    public String toString() {
        String s = "[ ";
        Nodo aux = L;
        while(aux != null) {
            s = s + aux + " | ";
            aux = aux.getLink();
        }
        return s + "]";
    }
    
    
}
