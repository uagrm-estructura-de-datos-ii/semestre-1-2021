package Examenes.PrimerParcial.T2_2020.Lista;

/**
 *
 * @author Ronaldo Rivero
 */
public class Nodo {
    public Par data;
    public Nodo link;

    public Nodo(Par data) {
        this.data = data;
        this.link = null;
    }

    public Par getData() {
        return data;
    }

    public void setData(Par data) {
        this.data = data;
    }

    public Nodo getLink() {
        return link;
    }

    public void setLink(Nodo link) {
        this.link = link;
    }        

    public boolean menor(Nodo b) {
        if(this.data.x < b.data.x) {
            return true;
        } else if(this.data.x == b.data.x) {
            if(this.data.y < b.data.y) {
                return true;
            }
        }
        return false;
    }
    
    public boolean igual(Nodo b) {
        return (this.data.x == b.data.x && this.data.y == b.data.y);
    }
    
    public boolean mayor(Nodo b) {
        if(this.data.x > b.data.x) {
            return true;
        } else if(this.data.x == b.data.x) {
            if(this.data.y > b.data.y) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        return "( "+data.x+", "+data.y+" )";
    }
}
