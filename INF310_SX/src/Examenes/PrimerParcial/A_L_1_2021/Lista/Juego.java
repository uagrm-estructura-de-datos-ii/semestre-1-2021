package Examenes.PrimerParcial.A_L_1_2021.Lista;

import java.util.Random;


public class Juego {       
    
    private Lista L;
    
    public Juego(){
        L = new Lista();
        Random random = new Random();
        while(L.length() < 3) {
            int valor = random.nextInt(9)+1;//( 0,1,2,3,4,5,6,7,8 ) + 1 = 1..9
            if(L.indexOf(valor) == -1) {
                L.add(valor);
            }
        }
        System.out.println("Naipes = " + L );
    }
    
    public boolean add(int nuevoNaipe, int naipeBotar){	
        int posBotar = L.indexOf(naipeBotar);
        if(posBotar != -1) {
            L.del(naipeBotar);
            L.add(nuevoNaipe);
            
            for(int i = 0; i < 2; i++) { //i = 0,1
                if(L.get(i+1) - L.get(i) != 1) {
                    return false;
                }
            }
            return true;
        }
        System.out.println("Naipes = " + L);
        return false;
    }
    
    @Override
    public String toString() {
        return L.toString();
    }
     
}
