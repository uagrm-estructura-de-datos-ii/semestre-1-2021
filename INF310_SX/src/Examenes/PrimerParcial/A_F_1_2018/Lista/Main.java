package Examenes.PrimerParcial.A_F_1_2018.Lista;


public class Main {
    private static final int[] datos = {11, 9, 5, 3, 12, 8, 10};
    
    private static void cargarDatos(Lista L){
        for (int i = 0; i < datos.length; i++) {
            L.add(datos[i]);
        }
    }
    
    
    public static void main(String[] args) {
        Lista p = new Lista();
        cargarDatos(p);
        System.out.println( p );
        //[3, 5, 8, 9, 10, 11, 12]
        p.invX(1, 2);
        System.out.println( p );
        //[3, 8, 5, 9, 10, 11, 12]
        p.invX(2, 4);
        System.out.println( p );
        //[3, 8, 11, 10, 9, 5, 12]
        
    }  
}
