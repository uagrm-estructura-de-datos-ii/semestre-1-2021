package Examenes.PrimerParcial.G_R_1_2019.Lista;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ronaldo Rivero
 */
public class Juego {
    
    private static final int N = 3;
    private List<ListaS> tarros;
    
    public Juego() {
        tarros = new ArrayList<>();
        for(int i = 0; i < N; i++) {
            tarros.add( new ListaS() );
        }
    }
    
    public void lanzar(int i, int moneda) {
        if(tarros.get(i).existe(moneda)){
            System.err.println("La moneda ya existe en el tarro"+i);
            return;
        }        
        tarros.get(i).insertar(moneda);
    }
    
    public boolean gano(int i) {
        ListaS l = tarros.get(i);
        int suma = 0;
        for(int j = 1; j <= l.longitud(); j++) {
            suma = suma + l.get(j);
        }        
        return suma==45;
    }
}
