/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Grafo;

import ADT_Grafo.excepciones.ExcepcionAristaYaExiste;
import ADT_Grafo.excepciones.ExcepcionNroVerticesInvalidos;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Veronica
 */
public class TestGrafo {
    public static void main(String argumentos[]) throws  ExcepcionAristaYaExiste{
        try {
            DiGrafo g = new DiGrafo(7) ;
            
            g.insertarArista(1, 3);
            g.insertarArista(1, 6);
            g.insertarArista(2, 4);                        
            g.insertarArista(3, 4);            
            
            g.insertarArista(0, 5);
            System.out.println(g.toString());
            ComponenteIslas islas = new ComponenteIslas(g);
            islas.mostrarComponenteDeIslas();            
        } catch (ExcepcionNroVerticesInvalidos ex) {
            Logger.getLogger(TestGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
