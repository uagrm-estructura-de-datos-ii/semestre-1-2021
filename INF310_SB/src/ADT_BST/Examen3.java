package ADT_BST;

public class Examen3<K extends Comparable<K>, V> extends Arbol<K,V>{
    
    public int cantidadNodosNivelN(int n) {
        return cantidadNodosNivelN(this.raiz, n, 0);
    }
    
    private int cantidadNodosNivelN(Nodo<K,V> p, int n, int a) {
        if(p == null)
            return 0;
        int c = cantidadNodosNivelN(p.getHijoIzquierdo(), n, a+1);
        c = c + cantidadNodosNivelN(p.getHijoDerecho(), n, a+1);
        if(a > n)
            c = c + 1;
        return c;
    }
}
