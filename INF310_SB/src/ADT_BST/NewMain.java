/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_BST;

/**
 *
 * @author Ronaldo Rivero
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(imp(5));
        System.out.println(imp2(5));
    }
    
    public static String imp(int n){
        if(n == 0)
            return "";
        
        String s = String.valueOf(n);
        s = s + imp(n-1);
        return s;
    }
    
    public static String imp2(int n){
        if(n == 0)
            return "";
        
        String s = imp2(n-1);
        s = s + String.valueOf(n);
        return s;
    }
    
}
