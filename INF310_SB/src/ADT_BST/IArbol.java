/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_BST;

/**
 *
 * @author Ronaldo Rivero
 */
public interface IArbol<K extends Comparable<K>,V> {
    
    void insertar(K clave, V valor) throws DatoYaExiste;    
    
    void inOrden();
    
    int altura();
    
    int cantNodoTrica();
    
    void invertir();
}
