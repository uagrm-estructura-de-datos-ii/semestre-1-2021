/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_BST;


/**
 *
 * @author Ronaldo Rivero
 */
public class Arbol<K extends Comparable<K>, V> implements IArbol<K, V> {
     
    protected Nodo<K, V> raiz;
    protected int n = 0;

    /**
     * funcion que devuelve True si el nodo es igual a null
     * @return 
     */
    public boolean esVacio() {
        return Nodo.esNodoVacio(raiz);       
    }
    
    /**
     * Inserta un valor V con la clave K, arbol el cual es ordenado
     * en base a K
     * @param clave
     * @param valor
     * @throws DatoYaExiste 
     */
    @Override
    public void insertar(K clave, V valor) throws DatoYaExiste {
        //clave es lo mismo X
        //El arbol esta vacio?
        if(esVacio()){ //Si esta vacio, lo construimo
            this.raiz = new Nodo(clave, valor);
        }else{
            Nodo<K, V> azul = null;
            Nodo<K, V> rojo = this.raiz;
            while(rojo != null) {
                azul = rojo;           
                //clave < rojo.getClave()
                if(clave.compareTo(rojo.getClave()) < 0)
                    rojo = rojo.getHijoIzquierdo();                   
                else{
                    //clave > rojo.getClave()
                    if(clave.compareTo(rojo.getClave()) > 0)
                        rojo = rojo.getHijoDerecho();
                    else{ 
                        //clave == rojo.getClave()
                        throw new DatoYaExiste("La clave ya existe " + clave);                        
                    }
                }
            }
            if (clave.compareTo(azul.getClave()) < 0){
                azul.setHijoIzquierdo(new Nodo(clave, valor));                
            }else{
                azul.setHijoDerecho(new Nodo(clave, valor));
            }
        }   
        this.n++;
        //Estas equilibrado? NO: equilibrate
        /*
        if (!estaEquilibrado())        
            equilibrar();
        */
    }

    /**
     * Procedimiento que recorre e imprime los elementos del
     * árbol de manera inOrden
     */
    @Override
    public void inOrden() {
        System.out.println("IN-ORDEN");
        if(esVacio()) {
            System.out.println("El arbol esta vacio");
        } else {
            inOrden1(raiz);
        }
    }
        
    private void inOrden1(Nodo<K, V> nodo) {
        if(!Nodo.esNodoVacio(nodo)){
            inOrden1(nodo.getHijoIzquierdo());
            System.out.println("[ "+ nodo.getClave()+"|"+ nodo.getValor()+ "]");
            inOrden1(nodo.getHijoDerecho());
        }      
    }              

    /**
     * función que devuelve la altura del árbol
     * recuerde que la altura de un árbol en igual
     * a la altura mayor de unos de sus hijos + 1
     * @return 
     */
    @Override
    public int altura() {
        return altura1(this.raiz);   
    }
      
    private int altura1(Nodo<K,V> nodoActual) {
      if(Nodo.esNodoVacio(nodoActual)) // como decir que n = 0
          return -1;   
      if(nodoActual.esHoja())
          return 0;
      int alturaPorIzqui = altura1(nodoActual.getHijoIzquierdo());
      int alturaPorDerecha = altura1(nodoActual.getHijoDerecho());
      return alturaPorIzqui > alturaPorDerecha ? alturaPorIzqui + 1 : alturaPorDerecha + 1;
    }
    
    private int cantNodo1(Nodo<K,V> nodoActual) {
        if(Nodo.esNodoVacio(nodoActual))
            return 0;
        if(nodoActual.esHoja())
            return 1;
        int cantPorIzquierdo = cantNodo1(nodoActual.getHijoIzquierdo());
        int cantPorDerecho = cantNodo1(nodoActual.getHijoDerecho());
        return cantPorIzquierdo+cantPorDerecho;
    }
    
    public boolean esLleno() {
        int h = altura1(raiz);
        int c = cantNodo1(raiz);
        return ((int)Math.pow(2, h))-1 == c;
    }
    
    private boolean esLleno1(Nodo<K,V> nodoActual) {
        if(Nodo.esNodoVacio(nodoActual))
            return true;
        if(nodoActual.esHoja())
            return true;
        
        boolean lleno  = true;
        if(cantNodo1(nodoActual.getHijoIzquierdo()) != cantNodo1(nodoActual.getHijoDerecho())) {
            lleno = false;
        }
        if(altura1(nodoActual.getHijoIzquierdo()) != altura1(nodoActual.getHijoDerecho())) {
            lleno = false;
        }
        
        boolean esLlenoIzq = esLleno1(nodoActual.getHijoIzquierdo());
        boolean esLlenoDer = esLleno1(nodoActual.getHijoDerecho());
        return lleno && esLlenoIzq && esLlenoDer;
    }
    
    private int factorEquilibrio(Nodo<K, V> nodo) {
        if(nodo == null)
            return 0;
        int alturaHI = altura1(nodo.getHijoIzquierdo());
        int alturaHD = altura1(nodo.getHijoDerecho());     
        return alturaHD - alturaHI;
    }
    
    public boolean estaEquilibrado() {
        return estaEquilibrado1(raiz);
    }        
    
    public boolean estaEquilibrado1(Nodo<K, V> nodo) {
        if(nodo == null)
            return true;
        
        if(nodo.esHoja())
            return true;
        
        int factor = factorEquilibrio(nodo);
        if (!(factor == -1 || factor == 0 || factor == 1))
            return false;
        
        boolean estaBalanceadoHI = estaEquilibrado1(nodo.getHijoIzquierdo());
        boolean estaBalanceadoHD = estaEquilibrado1(nodo.getHijoDerecho());
        return estaBalanceadoHI && estaBalanceadoHD;
    }
    
    private Nodo<K, V> rotacionSimpleIzquierdo(Nodo<K, V> nodo) {
        Nodo<K, V> nodoHD = nodo.getHijoDerecho();
        Nodo<K, V> nodoHDHI = nodoHD.getHijoIzquierdo();
        nodoHD.setHijoIzquierdo(nodo);
        nodo.setHijoDerecho(nodoHDHI);        
        return nodoHD;
    }
    
    private Nodo<K, V> rotacionSimpleDerecho(Nodo<K, V> nodo) {
        Nodo<K, V> nodoHI = nodo.getHijoIzquierdo();
        Nodo<K, V> nodoHIHD = nodoHI.getHijoDerecho();
        nodoHI.setHijoDerecho(nodo);
        nodo.setHijoIzquierdo(nodoHIHD);        
        return nodoHI;
    }
    
    private Nodo<K, V> rotacionDobleIzquierdo(Nodo<K, V> nodo) {
        nodo.setHijoDerecho(rotacionSimpleDerecho(nodo.getHijoDerecho()) );
        return rotacionSimpleIzquierdo(nodo);
    }
    
    private Nodo<K, V> rotacionDobleDerecho(Nodo<K, V> nodo) {
        nodo.setHijoIzquierdo(rotacionSimpleIzquierdo(nodo.getHijoIzquierdo()) );
        return rotacionSimpleDerecho(nodo);
    }
    
    private void equilibrar() {
        this.raiz = equilibrar1(this.raiz);
    }
    
    private Nodo<K, V> equilibrar1(Nodo<K, V> nodo) {
        if(nodo == null)
            return null;
        if(nodo.esHoja())
            return nodo;
        nodo.setHijoIzquierdo(equilibrar1(nodo.getHijoIzquierdo()));
        nodo.setHijoDerecho(equilibrar1(nodo.getHijoDerecho()));
        
        int feq = factorEquilibrio(nodo);
        int feqHI = factorEquilibrio(nodo.getHijoIzquierdo());
        int feqHD = factorEquilibrio(nodo.getHijoDerecho());
        if(feq == -2 && feqHI != 1)
            nodo = rotacionSimpleDerecho(nodo);
        if(feq == 2 && feqHD != -1)
            nodo = rotacionSimpleIzquierdo(nodo);
        if(feq == 2 && feqHD == -1)
            nodo = rotacionDobleIzquierdo(nodo);
        if(feq == -2 && feqHI == 1)
            nodo = rotacionDobleDerecho(nodo);
        return nodo;
    }

    @Override
    public int cantNodoTrica() {
        return cantNodoTrica(raiz);
    }
    
    private int cantNodoTrica(Nodo<K, V> p) {
        if(Nodo.esNodoVacio(p))
            return 0;
        if(p.esHoja())
            return 0;
        int cantHI = cantNodoTrica(p.getHijoIzquierdo());
        int cantHD = cantNodoTrica(p.getHijoDerecho());
        if(p.cantidadHijos() == 2)
            return cantHI + cantHD + 1;
        return cantHI + cantHD;
    }
    
    private Nodo<K, V> pregunta15(Nodo<K, V> p) {
        Nodo<K, V> aux = p.getHijoIzquierdo();
        while(aux.getHijoDerecho() != null) {
            aux = aux.getHijoDerecho();
        }
        return aux;
    }
    
    @Override
    public void invertir() {
        raiz = invertir(raiz);
    }
    
    private Nodo invertir(Nodo<K, V> p) {
        if(p == null)
            return null;        
        if(p.esHoja())
            return p;
        
        p.setHijoIzquierdo( invertir(p.getHijoIzquierdo()) );
        p.setHijoDerecho( invertir(p.getHijoDerecho()) );
        
        Nodo auxI = p.getHijoIzquierdo();
        Nodo auxD = p.getHijoDerecho();
        p.setHijoIzquierdo(auxD);
        p.setHijoDerecho(auxI);
        return p;
    }
    
    public boolean esMonticulo() {
        return esMonticulo(raiz);
    }
    
    private boolean esMonticulo(Nodo<K, V> p) {
        if(p == null)
            return false;
        if(p.esHoja())
            return false;
        
        if(p.cantidadHijos() == 2) {
            Nodo<K, V> HI = p.getHijoIzquierdo();
            Nodo<K, V> HD = p.getHijoDerecho();
            if(p.getClave().compareTo(HI.getClave()) < 0) {
                if(p.getClave().compareTo(HD.getClave()) < 0) {
                    return true;
                }
            }
            return false;
        }
        
        boolean a = esMonticulo(p.getHijoIzquierdo());
        if(a == true) {
            return true;
        }
        return esMonticulo(p.getHijoDerecho());
    }
}
