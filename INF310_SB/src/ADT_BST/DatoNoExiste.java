/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_BST;


/**
 *
 * @author Ronaldo Rivero
 */
public class DatoNoExiste extends Exception {

    public DatoNoExiste() {
        super("La clave no existe");
    }

    public DatoNoExiste(String mensaje) {
        super(mensaje);
    }
}
