package ADT_BST;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ronaldo Rivero
 */
public class MainExamen3 {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {       
        Examen3<Integer, String> arbol = new Examen3<Integer, String>();
        try {
            arbol.insertar(50, "50");
            arbol.insertar(25, "25");
            arbol.insertar(75, "75");
            arbol.insertar(10, "10");
            arbol.insertar(29, "29");
            arbol.insertar(27, "27");
        } catch (DatoYaExiste ex) {
            Logger.getLogger(Main2.class.getName()).log(Level.SEVERE, null, ex);
        }  
        System.out.println("Cantidad = "+ arbol.cantidadNodosNivelN(1));
    }
}
