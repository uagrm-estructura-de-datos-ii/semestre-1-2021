package ADT_BST;

import java.util.LinkedList;

public class Examen1<K extends Comparable<K>, V> extends Arbol<K,V>{
    
    public int cantidadNodosHojas() {
        if(Nodo.esNodoVacio(this.raiz))
            return 0;
        else
            return cantidadNodosHojas(this.raiz);
    }
    
    private int cantidadNodosHojas(Nodo<K, V> p) {
        int c = 0;
        LinkedList<Nodo<K,V>> nodos = new LinkedList<Nodo<K,V>>();
        nodos.addLast(p);
        do {
            Nodo<K,V> nodo = nodos.pop();
            if(nodo.esHoja())
                c++;
            else {
                if(nodo.esVacioHijoIzquierdo() != false)
                    nodos.addLast(nodo.getHijoIzquierdo());
                if(nodo.esVacioHijoDerecho() != false)
                    nodos.addLast(nodo.getHijoDerecho());
            }
        }while(nodos.size() > 0);
        return c;
    }
    
}
