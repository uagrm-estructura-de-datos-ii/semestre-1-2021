/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_MVIAS;

import ADT_BST.DatoYaExiste;
import ADT_BST.IArbol;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *
 * @author hp
 */
public class MVias<K extends Comparable<K>,V> implements IArbol<K,V> {
    
    protected NodoMVias<K,V>raiz;
    protected int orden;//M
    protected int POSICION_INVALIDA=-1;
    
    public MVias(){
        this.orden=3;
    }
    public MVias(int orden){
        if(orden<3){
            throw new RuntimeException("orden invalido");
        }
        this.orden=orden;
    }

    @Override
    public void insertar(K clave, V valor) throws DatoYaExiste {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void inOrden() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int altura() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int cantNodoTrica() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private K pregunta17(NodoMVias<K, V> p) {
        if(NodoMVias.esNodoVacio(p))
            return null;
        if(p.esHoja()) {
            K may = p.getClave(0);
            for(int i = 1; i < p.cantidadDeClavesNoVacias(); i++) {
                if(may.compareTo(p.getClave(i)) < 0) {
                    may = p.getClave(i);
                }
            }
        }
        
        K may = p.getClave(0);
        for(int i = 1; i < p.cantidadDeClavesNoVacias(); i++) {
            if(may.compareTo(p.getClave(i)) < 0) {
                may = p.getClave(i);
            }
        }
        
        for(int i = 0; i < orden; i++) {
            K m = pregunta17(p.getHijo(i));
            if (m != null) {
                if (may.compareTo(m) < 0)
                    may = m;
            }
        }
        return may;
    }
    
    public boolean pregunta18() {
        if(raiz == null)
            return false;
        int nivelRaiz = 3;
        return pregunta18(raiz, nivelRaiz, 0);
    }
    
    private boolean pregunta18(NodoMVias<K, V> p, int ultimo, int n) {
        if (NodoMVias.esNodoVacio(p))
            return true;
        
        if (p.esHoja())
            return true;
        
        if (ultimo == n) {
            if (p.esHoja() == false)
                return false;
            return true;
        }
        
        for (int i = 0; i < orden; i++) {
            if(pregunta18(p.getHijo(i), ultimo, n+1) == false)
                return false;
        }
        return true;
    }
    
    public boolean pregunta19() {
        if(raiz == null)
            return false;
        return pregunta19(raiz);
    }
    
    private boolean pregunta19(NodoMVias<K, V> p) {
        if (NodoMVias.esNodoVacio(p))
            return true;
        
        if (p.esHoja())
            return true;
        
        if(p.cantidadDeHijosNoVacios() < orden)
            return false;
        
        boolean esVerdadero = true;
        for(int i = 0; i < orden; i++) {
            if(pregunta19(p.getHijo(i)) == false)
                return false;
        }
        return true;        
    }
    
    
    public boolean esSimilar(MVias arbol) {
        return esSimilar1(raiz, arbol.raiz);
    }
    
    public boolean esSimilar1(NodoMVias<K, V> p, NodoMVias<K, V> p2) {
        if(NodoMVias.esNodoVacio(p) && NodoMVias.esNodoVacio(p2))
            return true;
        if(NodoMVias.esNodoVacio(p) || NodoMVias.esNodoVacio(p2))
            return false;
        
        if(p.esHoja() && p2.esHoja()) {
            if(p.cantidadDeClavesNoVacias() == p2.cantidadDeClavesNoVacias())
                return true;
            return false;
        }
        if(p.esHoja() || p2.esHoja())
            return false;        
        
        if(p.cantidadDeHijosNoVacios() != p2.cantidadDeHijosNoVacios()) {
            return false;
        }
                
        for(int i = 0; i < orden-1; i++) {
            if (p.getClave(i).compareTo(p2.getClave(i)) != 0)
                return false;
        }
                
        for(int i = 0; i < orden; i++) {
            if (esSimilar1(p.getHijo(i), p2.getHijo(i)) == false)
                return false;
        }
        return true;
    }
    
    public int cantHijoNoVacio(int n) {
        return cantHijoNoVacio(raiz, n, 0);
    }

    private int cantHijoNoVacio(NodoMVias p, int n, int x) {
	if(NodoMVias.esNodoVacio(p))
            return 0;	
	if(p.esHoja())
            return 0;        
	
	if(n == x) {
            return p.cantidadDeHijosNoVacios();
        }
        
        int c = 0;
        for(int i = 0; i < p.cantidadDeHijosNoVacios(); i++) {
            c = c + cantHijoNoVacio(p.getHijo(i), n, x+1);
        }
        return c;
    }

    @Override
    public void invertir() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
